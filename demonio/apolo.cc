/**
 * 
 * PROGRAMA DE GESTI�N DE LA PANTALLA, LECTURAS Y ESCRITURAS
 * 
 **/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <bits/stdc++.h>
#include <errno.h>
#include <string.h>
#include <cstdio>
#include <ctime>

#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <poll.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/syscall.h>
#include <sys/types.h>


////////////////////////////////////////////////////////////////////////////////
//////
//////	HILOS
//////


// MACROS
//

#define SIG1 SIGRTMIN+1
#define SIG2 SIGRTMIN+2

#define SIZE_BUFFER 1024 //TAMA�O DEL BUFFER DE COMUNICACI�N
#define TIMEOUT 1000 // milisegundos de espera en comunicaciones

#define SIZE_CARRETE 32
#define SIZE_MENSAJES 16
// TIPOS
//

typedef struct {
	char	buffer[SIZE_BUFFER];

	// Datos y estructura de un mensaje
	
	pid_t	pid;	// PID de Hermes
	char*	clave; // Apunta al interior de buffer
	char*	texto; // Apunta al interior de buffer

} mensaje;

enum { HILO_LILY, HILO_KERKAF };

typedef struct
{
    int ih;           // �ndice del hilo
    pthread_t *hs;    // Hilo al que debo esperar
                      // NULL == ninguno
		      //
    FILE* fd;	// Descriptor del canal bluetooth
} DatosHilo;

typedef struct DatoBluetooth {
	char	buffer[SIZE_BUFFER];

	char*	clave;	// Apunta al interior de buffer
	char*	texto;	// Apunta al interior de buffer

	DatoBluetooth() {  clave = texto = 0x0; }
	~DatoBluetooth() { }

} DatoBluetooth;

// DATOS
//
char direcCorreo[50];
char contenidoMensaje[500];

FILE* FDBT = 0x0;

bool sigUSR1 = false;
bool sigUSR2 = false;

pthread_mutex_t MemoriaMensajes = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t SemaforoCarrete = PTHREAD_MUTEX_INITIALIZER;

DatoBluetooth Carrete[SIZE_CARRETE];
int iCarreteW = 0;
int iCarreteR = 0;

mensaje Mensajes[SIZE_MENSAJES];
int iMensajeW = 0;
int iMensajeR = 0;

static const char* nameHilo[] = { "Padre", "Kerkaf" };
int tid_hilo[2];

pthread_t ID_padre;
pthread_t ID_Kerkaf;

////////////////////////////////////////////////////////////////////////////////
//////
//////	RUTINAS B�SICAS


pid_t Gettid()
{
    return syscall(SYS_gettid);
}

////////////////////////////////////////////////////////////////////////////////
//////
//////	MENSAJES
//////
//////
////// El formato de los mensajes es el siguiente:
//////
////// 	1.  Una clave (char) tomada de la lista anterior
////// 	2.  Un car�cter blanco ' ' (sirve de separador) o bien nada
////// 	3.  Datos ASCII separados por blancos
////// 	...
////// 	4.  El car�cter '\n'  (voy a utilizar fgets() para leer)
////// 	
////// No necesita el nulo de terminador de string.  El nulo no se env�a.
//////

// MACROS
//

//CODIGOS DE COMUNICACI�N PROTOCOLARIA
#define RTS 5 //ENQUIRY
#define CTS 6 //ACKNOWLEDGE
#define EOT 4 //END OF TRANSMISSION
#define BELL 7 //BELL (HE RECIBIDO Y GUARDADO)

// CLAVES DE LOS TIPOS DE MENSAJE
#define CRREO	'A'	// SE�AL DE ENVIAR CORREO

#define MENSAJEMSMTP "Subject: Alguien ha llamado a su despacho\n\nHan llamado a su puerta a las %02d:%02d del %2d-%d-%4d"

#define CPU_VIVA	'M'	// La CPU se ha activado
#define MUERTE_HILO	'Z'	// El hilo de lectura se ha muerto por
				// ruptura del canal BT




////////////////////////////////////////////////////////////////////////////////
//////
//////	MANEJO DE 'LILY'  (Pantalla de texto)
//////
//////	El demonio abre el canal Bluetooth y establece una comunicaci�n
//////	as�ncrona. Tambi� fija que esta comunicaci�n genere una se�al
//////	(que no va a ser SIGIO, sino SIG1).  El demonio fija la rutina
//////	de gesti�n de se�al: GestorLily
//////
//////
//////
//////	El GestorLily, cuando se invoca por medio de una se�al SIG1:
//////
//////		1. Crea un hilo que se encargar� de gestionar la lectura
//////		   del mensaje y actuar en consecuencia.
//////		2. Cuando termina genera una se�al SIGALARM para que el
//////		   demonio sepa que ha terminado.
//////		3. El hilo muere sin juntarse con nadie.

int procesaMensaje(const char* tH, DatoBluetooth* db) { 
    char* sig = 0x0;
    char* longitud = strtok_r(db->buffer," ",&sig);
    char* t_clave = strtok_r(sig," ",&sig);
    char* t_datos = strtok_r(sig,"\n",&sig);
    if(longitud == NULL || t_clave == NULL || t_datos == NULL){
        fprintf(stderr,"[%s] error en los datos\n",tH);
        return -1; //datos en formato incorrecto
    } 
    int tam = atoi(longitud);
    if(strlen(t_datos) != tam || tam > 256){
        fprintf(stderr,"[%s] error en la longitud de los datos (%d):%s:\n",tH, tam, t_datos);
        return -1; //datos en formato incorrecto
    } 
    db->clave = t_clave;
    db->texto = t_datos;
    return 1;
}

void procesaCarrete(const char* tH){
    pthread_mutex_lock(&SemaforoCarrete);
    while(iCarreteW != iCarreteR){
        pthread_mutex_unlock(&SemaforoCarrete);
        printf("[%s] he le�do del carrete \"%d\" :%s:%s:%s:\n", tH, iCarreteR, Carrete[iCarreteR].buffer, Carrete[iCarreteR].clave, Carrete[iCarreteR].texto);

        if(*Carrete[iCarreteR].clave == CRREO){
            printf("[%s] se�al de enviar correo\n", tH);

            //fecha y hora
            std::time_t t = std::time(0);   // get time now
            std::tm* now = std::localtime(&t);
            sprintf(contenidoMensaje, MENSAJEMSMTP, now->tm_hour, now->tm_min, now->tm_mday, now->tm_mon+1, now->tm_year+1900);
            printf("[%s] Mensaje a enviar:  :%s:\n", tH, contenidoMensaje);

            //correo electr�nico adecuado
            FILE* direcCorreoFile = fopen("../config/mail.txt", "r");
            if(direcCorreoFile == NULL){
                printf("[%s] error abriendo el fichero mail.txt\n", tH);
            } else {
                if(fgets(direcCorreo, 1024, direcCorreoFile)){ //comprobamos que no haya errores y mandamos el correo
                    printf("[%s] Direcci�n de correo:  :%s:\n", tH, direcCorreo);
                    char comandoCorreo[550];
                    sprintf(comandoCorreo, "echo \"%s\" | sudo msmtp -C /etc/msmtp/config %s", contenidoMensaje, direcCorreo);
                    printf("[%s] Comando a ejecutar:  :%s:\n", tH, comandoCorreo);
                    system(comandoCorreo);
                }else{
                    printf("[%s] error en la lectura de direcci�n de correo\n", tH);
                }
            }
        }
        iCarreteR = (++iCarreteR)%SIZE_CARRETE; 
        pthread_mutex_lock(&SemaforoCarrete);
    }
    pthread_mutex_unlock(&SemaforoCarrete);
}


void vaciarCom(const char* tH, FILE* fd){
    char borrado[SIZE_BUFFER];
    printf("[%s] borramos :",tH);
    if(fgets(borrado, SIZE_BUFFER, fd) == NULL){
        printf("\n[%s] Error al borrar el buffer :",tH);
    }else{
        printf(":%s:\n", borrado);
    }
}


int mandaCaracter(const char* tH, int c, FILE* fd){
    struct pollfd PollFd;
    PollFd.fd = fileno(fd);
    PollFd.events = POLLOUT;
    PollFd.revents = 0;

    int res = poll(&PollFd, 1, TIMEOUT);
    if (res < 0) {
	//  ERROR
	fprintf(stderr,"[%s] ",tH);
	perror("Error en el env�o de un car�cter");
	return -1;
    } else if (res == 0) {
	//  TIMEOUT
	fprintf(stderr,"[%s] Tiempo de espera excedido en la espera del env�o de un car�cter\n",tH);
	return -2;
    } else {
	// Hay un car�cter en espera
	// Leo el car�cter
	int cc = fputc(c,fd);
    fflush(fd);
	return cc;
    }
}


int esperaCaracter(const char* tH, FILE* fd)
{
    struct pollfd PollFd;
    PollFd.fd = fileno(fd);
    PollFd.events = POLLIN;
    PollFd.revents = 0;

    int res = poll(&PollFd, 1, TIMEOUT);
    if (res < 0) {
	//  ERROR
	fprintf(stderr,"[%s] ",tH);
	perror("Error en la espera de un car�cter");
	return -1;
    } else if (res == 0) {
	//  TIMEOUT
	fprintf(stderr,"[%s] Tiempo de espera excedido en la espera de un car�cter\n",tH);
	return -2;
    } else {
	// Hay un car�cter en espera
	// Leo el car�cter
    
	int c = getc(fd);
    printf("[%s] esperarCaracter recibe: %d\n",tH,c);
	return c;
    }
}


int esperaTexto(const char* tH, char* sitio, int size, FILE* fd)
{
    struct pollfd PollFd;
    PollFd.fd = fileno(fd);
    PollFd.events = POLLIN;
    PollFd.revents = 0;

    int res = poll(&PollFd, 1, TIMEOUT);
    if (res < 0) {
	//  ERROR
	fprintf(stderr,"[%s] ",tH);
	perror("Error en la espera de un texto");
	return -1;
    } else if (res == 0) {
	//  TIMEOUT
	fprintf(stderr,"[%s] Tiempo de espera excedido en la espera de un texto\n",tH);
	return -2;
    } else {
	// Hay datos en el canal
	// Leo hasta que encuentre '\n'
	if (fgets(sitio,size,fd)){
	    return 0;
	} else {
	    fprintf(stderr,"[%s] Error en la lectura de un texto\n",tH);
	    return -3;
	    }    
    }
}

// GESTOR DE INTERRUPCI�N SIG1
// Se le invoca mediante una se�al que genera el propio canal Bluetooth

void GestorLily(int sig, siginfo_t *si, void *ucontext)
{
    static int vez = 0;
    vez++;
    sigUSR1 = true;

    pid_t mipid = getpid();
    pid_t mitid = Gettid();
    const char* tH = (mipid == mitid ?  nameHilo[0] : nameHilo[1]);

    printf("[%s] {1} Entro en el gestor de lectura : %d pid=%d fd=%d\n"
		    ,tH, vez, si->si_pid,si->si_fd);

    if (si->si_pid == mipid) { 
        printf("[%s] me han invocado desde dentro\n",tH);
	// Me han invocado desde dentro: hay mensajes de hermes
	pthread_mutex_lock(&MemoriaMensajes);
	bool deboEscribir = (iMensajeR != iMensajeW);
	pthread_mutex_unlock(&MemoriaMensajes);
	if (deboEscribir) {
	   // Mando un RTS a la Lily
	   int res = mandaCaracter(tH, RTS, FDBT);
	   return;
	} else return; // No s� qu� ha pasado.
    } else {
	// Me llaman desde el exterior
    // Preparo el almacenamiento
    pthread_mutex_lock(&SemaforoCarrete);
	DatoBluetooth* db = &Carrete[iCarreteW];
    pthread_mutex_unlock(&SemaforoCarrete);

	// Me pongo a la escucha del canal de LiLy
	int c = esperaCaracter(tH, FDBT);
	if(c < 0) {
	    printf("[%s] problemas caracter inicial\n",tH);
	    return;
	}

	switch (c) {
	    case RTS: {
	        printf("[%s] mando un CTS\n",tH);
		int res = mandaCaracter(tH, CTS, FDBT);
		if (res == EOF) {
		    fprintf(stderr,"[%s] [1] Error al contestar a Lily\n",tH);
		    return;
		}
				
		res = esperaTexto(tH, db->buffer, SIZE_BUFFER, FDBT);
		if (res < 0) return;

		// Ha le�do bien. Espero el EOT
		int cc = esperaCaracter(tH, FDBT);
		if (cc != EOT) {
		    fprintf(stderr,"[%s] No recibo EOT\n",tH);
		    return;
		}
        //comprobamos el texto y se guarda en sus campos
        res = procesaMensaje(tH, db);
        if(0 > res) return;
		// Le confirmo que he recibido todo bien
		res = mandaCaracter(tH, BELL,FDBT);
		if (res == EOF) {
		    fprintf(stderr,"[%s] [2] Error al contestar a Lily\n",tH);
		    return;
		}

		// Todo ha ido bien
		printf("[%s] todo ha ido bien y he recibido:%s %s %s:dejado en %d\n\n\n",
				tH, db->buffer, db->clave, db->texto,iCarreteW);
        pthread_mutex_lock(&SemaforoCarrete);
		iCarreteW = (++iCarreteW)%SIZE_CARRETE;
        pthread_mutex_unlock(&SemaforoCarrete);
        
		return;
	    } break;

	    case CTS: {
		    
            printf("[%s] Ha llegado el CTS\n",tH);
            mensaje* M = &Mensajes[iMensajeR];
            fprintf(FDBT,"%d %s %s\n%c",strlen(M->texto),M->clave,M->texto,(char)EOT);
            fflush(FDBT);
            int res = esperaCaracter(tH, FDBT);

            if (res != BELL) {
            printf("[%s] [GestorLily] No he recibido un BELL\n",tH);
            union sigval valor;	// No se usa
            pthread_sigqueue(ID_padre, SIG1, valor); //repetimos el intento
            return;
            } else { //Avisamos al hermes de que todo ha ido chachi
            pid_t p = Mensajes[iMensajeR].pid;
            iMensajeR = (++iMensajeR)%SIZE_MENSAJES;
            kill(p,SIG2);
            return;
            }
	    } break;

	    default: {
            fprintf(stderr,"[%s] Caracter recibido sin sentido [%d %c]\n",tH,c,(char) c);
		    vaciarCom(tH, FDBT);
            return;
            }
	  }
    }

   // Creaci�n del hilo de lectura
   //pthread_t* H = new pthread_t();
   //if (pthread_create(H, NULL, HiloLecturaLily, FDBT)) {
   //perror("No he podico crear el hilo de lectura Kerkaf : ");
   //} else {}
}


////////////////////////////////////////////////////////////////////////////////
//////
//////	MANEJO DE 'KERKAF' (Tel�fono)
//////
//////	Se activa mediante la se�al SIG2 que la env�a 'hermes'
//////	Cuando se recibe la se�al se genera un hilo que ser� el encargado
//////	de leer el mensaje y actuar en consecuencia.
//////

const char* nombre_shm = "memoria_apolo";

// GESTOR DE INTERRUPCI�N SIG2
// Se le invoca cuando hay un mensaje de 'hermes' (el tel�fono ha llamado)
//
void GestorKerkaf(int, siginfo_t*, void *)
{
    static int vez = 0;
    vez++;
    sigUSR2 = true;

    pid_t mipid = getpid();
    pid_t mitid = Gettid();
    pid_t tidPadre = mipid;
    const char* tH = (mipid == mitid ?  nameHilo[0] : nameHilo[1]);

    // Habilito la memoria compartida
    //
    int id_shm = shm_open(nombre_shm,O_RDWR, 0777);
    if (id_shm < 0) {
	fprintf(stderr,"[%s] No he podido crear la memoria compartida\n",tH);
	exit(-1);
    }
    char* Memoria = (char*) mmap(NULL, 1024,
		    		PROT_READ|PROT_WRITE, MAP_SHARED, id_shm, 0);

    printf("[%s] He recibido :%s:\n",tH,Memoria);
    if(Memoria[0] == 'R'){
        union sigval valor;
        pthread_sigqueue(ID_padre, SIG1, valor);
        return;
    }
    // Copio lo le�do a mi buffer
    pthread_mutex_lock(&MemoriaMensajes);
      mensaje* M = &Mensajes[iMensajeW];
      strncpy(M->buffer,Memoria,SIZE_BUFFER);

      char* sig = 0x0;
      M->pid = atoi(strtok_r(M->buffer," ",&sig));
      M->clave = strtok_r(sig," ",&sig);
      M->texto = sig;
      iMensajeW = (++iMensajeW)%SIZE_MENSAJES;
    pthread_mutex_unlock(&MemoriaMensajes);

    printf("[%s] pid = :%d:\n",tH,M->pid);
    printf("[%s] clave = :%c:\n",tH,M->clave[0]);
    printf("[%s] datos = :%s:\n",tH,M->texto);


    // Comunico a Hermes que todo ha ido bien
    sprintf(Memoria,"[%s] Todo ha ido bien\n",tH);
    union sigval valor;	// No se usa
    sigqueue(M->pid, SIG1, valor);
// ANTES:  kill(M->pid,SIG1);
    shm_unlink(nombre_shm);

    // Comunico al Gestor de Lily que tengo un mensaje pendiente
// ANTES   kill(getpid(),SIG1);
    pthread_sigqueue(ID_padre, SIG1, valor);
}

// HILO DE LECTURA Kerkaf
//
void* HiloKerkaf(void *a)
{
    ID_Kerkaf = pthread_self();
    printf("[Kerkaf] espero\n"); fflush(stdout);
    while(1) {
	pause();
	printf("[kerkaf] vuelvo al pause\n"); fflush(stdout);
    }
}

////////////////////////////////////////////////////////////////////////////////
//////
//////	GESTI�N DE LA SALIDA

void GestorSalida(int s)
{
    system("sudo /home/pi/Desktop/TFG/workspace/raspi-bff/exe_path/release.sh");
}


////////////////////////////////////////////////////////////////////////////////
//////
//////	GESTI�N DE LAS ALARMAS

void GestorAlarma(int sig, siginfo_t *si, void *ucontext)
{
    printf("Alarma recibida\n");
}


////////////////////////////////////////////////////////////////////////////////
//
//  PROGRAMA PRINCIPAL.
//
////////////////////////////////////////////////////////////////////////////////

using namespace std;

int main(int argc, char **argv)
{
static const char* ligamento = "sudo /home/pi/Desktop/TFG/workspace/raspi-bff/exe_path/bindear.sh";
static const char* mac       = "7C:9E:BD:FB:18:86";
static const char* abreBluetooth = "sudo /home/pi/Desktop/TFG/workspace/raspi-bff/exe_path/bindear.sh 7C:9E:BD:FB:18:86";


///////////////////////////////////////////////////////////////// ME IDENTIFICO
    ID_padre = pthread_self();

///////////////////////////////////////////////////////////////////// AL ACABAR
    signal(SIGTERM,GestorSalida);

////////////////////////////////////////////////////////////////////////// LILY

    // Apertura del canal Bluetooth
    system(abreBluetooth);
    FDBT = fopen("/dev/rfcomm1", "r+");
    if (NULL == FDBT) {
	fprintf(stderr,"[Main] ERROR: no he podido abrir el canal Bluetooth\n");
	exit(-1);
    }

    // Establezco el protocolo del canal Bluetooth
    int fdBT = fileno(FDBT);
    int a;
    int flagsFL = fcntl(fdBT, F_GETFL, a);
    int nFlag = flagsFL | O_ASYNC;
    int res = fcntl(fdBT, F_SETFL, nFlag);
    int pid = getpid();
    int tid = Gettid();
    struct f_owner_ex owner;
    owner.type = F_OWNER_TID;
    owner.pid  = tid;
    res = fcntl(fdBT, F_SETOWN_EX, &owner);
    res = fcntl(fdBT, F_SETSIG, SIG1);

    // Pongo el gestor de la se�al SIG1
    struct sigaction saLily;
    saLily.sa_flags = SA_RESTART|SA_SIGINFO;
    sigemptyset(&saLily.sa_mask);
    saLily.sa_sigaction = GestorLily;
    if (-1 == sigaction(SIG1, &saLily, NULL))
    {
        perror("[Main] Error al establecer sigaction Lily\n");
        exit(-1);
    }

//////////////////////////////////////////////////////////////////////// KERKAF

    // CREO EL HILO ENCARGADO DE LA LECTURA-ESCRITURA DE KERKAF
    //
    pthread_t H_Kerkaf;
    if (pthread_create(&H_Kerkaf, NULL, HiloKerkaf, NULL)) {
        perror("[Main] No he podico crear el hilo Kerkaf : ");
        exit(-1);
    }

    // ESTABLEZCO EL GESTOR DE SE�AL SIG2
// ANTES:    signal(SIG2,GestorKerkaf);
    struct sigaction saKerkaf;
    saKerkaf.sa_flags = SA_RESTART|SA_SIGINFO;
    sigemptyset(&saKerkaf.sa_mask);
    saKerkaf.sa_sigaction = GestorKerkaf;
    if (-1 == sigaction(SIG2, &saKerkaf, NULL))
    {
        perror("[Main] Error al establecer sigaction Kerkaf\n");
        exit(-1);
    }


/////////////////////////////////////////////////////////////////////// ALARMAS

    // Pongo el gestor de la se�al SIGALRM
    struct sigaction saAlarma;
    saAlarma.sa_flags = SA_RESTART|SA_SIGINFO;
    sigemptyset(&saAlarma.sa_mask);
    saAlarma.sa_sigaction = GestorAlarma;
    if (-1 == sigaction(SIGALRM, &saAlarma, NULL))
    {
        perror("[Main] Error al establecer sigaction Alarma\n");
        exit(-1);
    }

    // ME PONGO A LA ESCUCHA
    do
    {
        printf("[Main] Me pongo a la escucha\n\n\n-------------------------------------\n");
        fflush(stdout);
        pause();
        printf("[Main] He salido del pause\n");
        fflush(stdout);
	if (sigUSR1) {
	    printf("[Main] he procesado lily\n");
        pthread_mutex_lock(&SemaforoCarrete);
	    if(iCarreteW != iCarreteR){ //vigilamos con el mutex no tropezar sobre iCarreteW ya que es otro hilo el de escucha de lily
            pthread_mutex_unlock(&SemaforoCarrete);
	        procesaCarrete(nameHilo[0]);
        }
        pthread_mutex_unlock(&SemaforoCarrete);

	   // Pendiente de bloque de memoria
	   sigUSR1 = false;
	}
	if (sigUSR2) {
	   printf("[Main] He recibido una se�al para Kerkaf !!!!!\n");

	   // Pendiente de bloqueo de memoria
	   sigUSR2 = false;
	}
    } while (1);


    system("sudo /home/pi/Desktop/TFG/workspace/raspi-bff/exe_path/release.sh");

    return 0;
}
