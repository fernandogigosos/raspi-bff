///////////////////////////////////////////////////////////////////////////
//
// Programa de prueba de memoria compartida.
//
// Este programa (que deber�a ser un demonio) atiende las se�ales SIGUSR2
// para leer una memoria compartida
//
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/types.h>
#include <signal.h>

const char* nombre_shm = "memoria_apolo";

void HayDatos(int s)
{
    // Creo la memoria compartida
    //
    int id_shm = shm_open(nombre_shm,O_RDWR, 0777);
    if (id_shm < 0) {
	fprintf(stderr,"No he podido crear la memoria compartida\n");
	exit(-1);
    }
    char* Memoria = (char*) mmap(NULL, 1024,
		    		PROT_READ|PROT_WRITE, MAP_SHARED, id_shm, 0);

    printf("He recibido :%s:\n",Memoria);

    char* sig = 0x0;
    char* t_pid = strtok_r(Memoria," ",&sig);
    char* t_clave = strtok_r(sig," ",&sig);
    char* t_datos = sig;

    int pid = atoi(t_pid);
    printf("pid = %d\n",pid);
    printf("clave = %c\n",t_clave[0]);
    printf("datos = %s\n",t_datos);

    sprintf(Memoria,"Todo ha ido bien");
    kill(pid,SIGUSR1);
    shm_unlink(nombre_shm);
}


void manual(int na, char* arg[])
{
   exit(-1);
}

int main(int na, char* arg[])
{
    static char sitio[1024];

    // Configuro la se�al SIGUSR2
    signal(SIGUSR2,HayDatos);

    // Me pongo a la espera
    do {
	printf("Me pongo a la espera\n");
	pause();
    } while (1);
}
