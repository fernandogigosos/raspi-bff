///////////////////////////////////////////////////////////////////////////
//
// Programa de prueba de memoria compartida.
//
// Este programa se ejecutar� a distancia (mediante un ssh).  Se encargar�
// de
// 	1. Averiguar el pid del programa "apolo"
// 	2. Crear una memoria compartida para comunicarse con "apolo"
// 	3. Enviar una se�al a "apolo" cuando la memoria est� escrita
// 	4. Esperar a que "apolo" la confirme que lo ha le�do.
// 	5. Acabar.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <sys/types.h>
#include <signal.h>

#define SIG1 SIGRTMIN + 1
#define SIG2 SIGRTMIN + 2

static const char *path_apolo = "/home/pi/Desktop/TFG/workspace/raspi-bff/demonio";
static const char *apolo = "apolo";
//static const char* apolo = "hestia";
static const char *nombre_shm = "memoria_apolo";

bool SIG1recibida = false;
bool SIG2recibida = false;

void DatoLeido(int)
{
    SIG1recibida = true;
    printf("[Hermes] Dato Le�do.\n");
    fflush(stdout);
}

void DatoGuardado(int)
{
    SIG2recibida = true;
    printf("[Hermes] Dato Enviado.\n");
    fflush(stdout);
}

void manual(int na, char *arg[])
{
    fprintf(stderr, "\nUtiliza\n\t% s"
                    " <clave: una letra> \"Texto a enviar (entre comillas)\"\n"
                    "\n\n",
            arg[0]);
    exit(-1);
}

void relanzaApolo()
{
    SIG1recibida = false;
    static char orden[64];
    sprintf(orden, "nohup %s/%s 1>./nohup.out 2>&1 &", path_apolo, apolo);
    printf("ORDEN :%s:\n", orden);
    system(orden);
    printf("Relanzo apolo\n");
    sleep(5);
}

int main(int na, char *arg[])
{
    static char sitio[1024];

    if (na != 3)
        manual(na, arg);
    if (strlen(arg[1]) > 1)
        manual(na, arg);

    // Configuro la se�al SIG1
    signal(SIG1, DatoLeido);

    // Configuro la se�al SIG2
    signal(SIG2, DatoGuardado);

    int intentos = 0;
    int nh;
    int PID[8];
    int TID[8];
    do
    {
        static char orden[256];

        intentos++;

        // Averiguo el pid de 'apolo'
        sprintf(sitio, "ps -L -C %s -o pid= -o lwp=", apolo);
        FILE *fds = popen(sitio, "r");
        if (NULL == fds)
        {
            fprintf(stderr, "No he podido averiguar el pid de %s\n", apolo);
            exit(-1);
        }

        nh = 0;
        sitio[0] = 0;
        while (fgets(sitio, sizeof(sitio), fds))
        {
            if (sitio[0] == 0)
            {
                // Lanzo apolo
                relanzaApolo();
                sleep(intentos);
                continue;
            }

            sscanf(sitio, "%d %d", &PID[nh], &TID[nh]);
            nh++;
            if (nh == 8)
                break;
        }
        if (nh == 0)
        {
            relanzaApolo();
            sleep(intentos);
        }
        if (nh == 1)
        {
            printf("No hay dos hilos de apolo\n");
            // Mato apolo
            int res = kill(PID[0], SIGKILL);
            if (res != 0)
            {
                perror("No he podido detener apolo : ");
                exit(-1);
            }
            // Lo vuelvo a lanzar
            relanzaApolo();
            sleep(intentos);
            continue;
        }
        else if (nh == 2)
        {
            pclose(fds);

            pid_t pid_apolo = PID[0];
            pid_t pid_hilo;
            for (int i = 0; i < nh; i++)
            {
                if (PID[i] != TID[i])
                {
                    pid_hilo = TID[i];
                    break;
                }
            }
            // printf("pid de %s : %d\n",apolo,pid_apolo); fflush(stdout);

            // Creo la memoria compartida
            //
            // printf("size debe ser m�ltiplo de %d\n",sysconf(_SC_PAGE_SIZE));
            //size_t size_M = strlen(arg[2])+10; // 10: <mi_pid %6> <clave><blanco> <\0>
            size_t size_M = sysconf(_SC_PAGE_SIZE);
            int id_shm = shm_open(nombre_shm, O_RDWR | O_CREAT, 0777);
            if (id_shm < 0)
            {
                fprintf(stderr, "No he podido crear la memoria compartida\n");
                exit(-1);
            }

            char *Memoria = (char *)mmap(NULL, size_M,
                                         PROT_READ | PROT_WRITE, MAP_SHARED, id_shm, 0);
            // printf("&Memoria = %x\n",Memoria); fflush(stdout);

            // Escribo el dato
            dprintf(id_shm, "%6d %c %s\0", getpid(), arg[1][0], arg[2]);

            // Aviso a "apolo" que ya est� todo listo
            union sigval valor; // No se usa
            if (0 != sigqueue(pid_hilo, SIG2, valor))
            {
                fprintf(stderr, "No he podido enviar la se�al a %s\n", apolo);
                exit(-1);
            }

            // Me pongo a la espera de una se�al
            while (!SIG1recibida)
            {
                pause();
            }

            if (!SIG2recibida)
            {
                int resto = sleep(4);
                if (resto > 0)
                    break;
                int res = kill(PID[0], SIGKILL);
                if (res != 0)
                {
                    perror("No he podido detener apolo : ");
                    exit(-1);
                }                
                relanzaApolo();
                intentos++;
                sleep(intentos);
            } else {
                break;
            }
            // Destruyo la memoria
            shm_unlink(nombre_shm);


        }
    } while (intentos < 10);
    if (nh < 2)
    {
        printf("Programa fallido\n");
        exit(-1);
    }
}
